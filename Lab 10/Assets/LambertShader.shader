﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/LambertShader" {
	Properties {
		_Color("Color", Color) = (1,1,1,1)
		_SpecColor("Specular Color", Color) = (1,1,1,1)
		_Shininess("Shininess", float) = 10
	}
	SubShader
	{
		pass
		{
			Tags{"LightMode" = "ForwardBase"}

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;

			uniform float4 _LightColor0;

			struct vertexInput
			{
				float4 vertexPos : POSITION; // position of the vertex
				float3 vertexNormal : NORMAL;
			};

			struct vertexOutput
			{
				float4 pixelPos : SV_POSITION; //position of the vertex
				float4 pixelCol : COLOR;

				float3 normalDirection : TEXCOORD0;
				float4 pixelWorldPos : TEXCOORD1;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput toReturn;

				float3 normalDirection = normalize(mul(float4(input.vertexNormal, 0.0), unity_ObjectToWorld).xyz);
				float3 viewDirection = normalize(float3(float4(_WorldSpaceCameraPos.xyz, 1.0) - mul(unity_ObjectToWorld, input.vertexPos).xyz));

				toReturn.pixelWorldPos = float4(viewDirection, 1.0);
				toReturn.normalDirection = normalDirection;

				toReturn.pixelPos = UnityObjectToClipPos(input.vertexPos);


				return toReturn;
			}

			half4 frag(vertexOutput input) : COLOR
			{
				float3 lightDirection;
				float attenuation = 1.0;
				lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float3 diffuseReflection = attenuation * _SpecColor.rgb * max(0.0,dot(input.normalDirection, lightDirection));

				float3 specularReflection = reflect(-lightDirection, input.normalDirection);
				specularReflection = dot(specularReflection, input.pixelWorldPos);
				specularReflection = max(0.0, specularReflection);
				specularReflection = max(0.0, dot(input.normalDirection, lightDirection)) * specularReflection;
				specularReflection = pow(max(0.0, specularReflection), _Shininess);

				float3 finalLight = specularReflection + diffuseReflection + UNITY_LIGHTMODEL_AMBIENT;
				input.pixelCol = float4(finalLight * _Color, 1.0);

				return input.pixelCol;
			}

			ENDCG
		}
	}

}
